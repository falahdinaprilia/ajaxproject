from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
import requests
from django.urls import reverse
# Create your views here.

def home(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        if not 'book' in request.session.keys():
            request.session['book'] = [id]
            hasil = 1
        else:
            books = request.session['book']
            books.append(id)
            request.session['book'] = books
            hasil = len(books)
        return JsonResponse({'count': hasil, 'id': id})
    if 'book' in request.session.keys():
        listBook = request.session['book']
    else:
        listBook = []
    nama = request.session['name']
    return render(request, 'homepage.html', {'count': len(listBook), 'nama':nama })


def getData(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    query = request.GET["q"]
    get = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + query).json()
    hasil = get['items']
    newlist = []
    for items in hasil:
        dataId = items['id']
        if dataId in request.session['book']:
            bool = True
        else:
            bool = False
        newdict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "published": items['volumeInfo']['publishedDate'],
                   'id': items['id'], 'boolean': bool}
        newlist.append(newdict)
    return JsonResponse({'data': newlist})


def delete(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        hasil = request.session['book']
        hasil.remove(id)
        count = len(hasil)
        request.session['book'] = hasil
        return JsonResponse({'count': count})
