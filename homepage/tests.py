from importlib import import_module

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from isort import settings

from .views import home
import requests
# Create your tests here.

class SessionTestCase(TestCase) :
    def set_up(self):
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

class FavoriteUnitTest(SessionTestCase):
    def test_favorite_logged_in(self):
        c = self.client.session
        c['user_id']='test'
        c['name']='dnnn'
        c.save()
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)

    def test_logged_in_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_client_can_POST_and_render(self):
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'dnnn'
        s.save()
        response_post = self.client.post('/', {'id': 'abcd'})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_delete_and_delete_selected(self):
        title = "hehehe"
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'dnnnn'
        s['book'] = [title]
        s.save()
        response_post = self.client.post('/getdata?q=quiliting', {'id': title})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_get_data(self):
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'hany'
        s['book'] = []
        s.save()
        response = self.client.get('/getdata?q=quiliting')
        self.assertEqual(response.status_code, 200)

    def test_books_not_favorited(self):
        s = self.client.session
        s['user_id'] = 'test'
        s['name'] = 'dnnn'
        s['book'] = []
        s.save()
        response = self.client.get('/getdata?q=quiliting').json()
        bool = response['data'][0]['boolean']
        self.assertFalse(bool)



# class Lab9UnitTest(TestCase):
#
#     def test_apakah_halaman_ada_atau_tidak(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code, 200)
#
#     def test_apakah_menggunakan_fungsi(self):
#         found = resolve('/')
#         self.assertEqual(found.func, home)
#
#     def test_model_bisa_menambahkan_favorite(self):
#         Favorites.objects.create(title='a', id='abcd')
#         count = Favorites.objects.count()
#         self.assertEqual(count,1)
#
#     def test_client_bisa_post_dan_render(self):
#         title = "hehehe"
#         response_post = Client().post('/', {'title': title, 'id': 'abcd'})
#         self.assertEqual(response_post.status_code, 200)
#
#     def test_client_bisa_menghapus_dan_menghapus_yang_dipilih(self):
#         title = "hehehe"
#         Client().post('/', {'title': title, 'id': title})
#         response_post = Client().post('/delete', {'id': title})
#         self.assertEqual(response_post.status_code, 200)
#         response = Client().get('/')
#         html_response = response.content.decode('utf8')
#         self.assertNotIn(title, html_response)
#
#     def test_client_tidak_bisa_menghapus_apabila_id_tidak_sama(self):
#         title = "abc"
#         Client().post('/', {'title': title, 'id': title})
#         response_post = Client().post('/delete', {'id': 'olala'})
#         self.assertEqual(response_post.status_code, 404)
#
#     def test_client_bisa_get_data(self):
#         response = Client().get('/getdata?q=quilting')
#         self.assertEqual(response.status_code, 200)
#
#     # def test_buku_sudah_menjadi_favorite(self):
#     #     get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
#     #     items = get['items']
#     #     book = items[0]
#     #     dataId = book['id']
#     #     title = book['volumeInfo']['title']
#     #     Client().post('/', {'title': title, 'id': dataId})
#     #     response = Client().get('/getdata').json()
#     #     bool = response['data'][0]['boolean']
#     #     self.assertTrue(bool)
#
#     def test_buku_tidak_favorite(self):
#         response = Client().get('/getdata?q=quilting').json()
#         bool = response['data'][0]['boolean']
#         self.assertFalse(bool)
