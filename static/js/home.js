$(document).ready(function () {

    $(".kategori").click(function () {
        var id = this.id;
        $.ajax({
            method: "GET",
            url: "/getdata?q=" + id,
            success: function (hasil) {
                var data = hasil.data;
                $('td').remove(html);
                for (var x = 0; x < data.length; x++) {
                    var dataBook = data[x];
                    var title = dataBook.title;
                    if (dataBook.boolean) {
                        var button = '<button' + ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="deleteFavorite(\'' + dataBook.id + '\')">' +'<i class="del"></i></button>';
                    } else {
                        var button = '<button'+ ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="addFavorite(\'' + name + '\', \'' + dataBook.id + '\')">' +'<i class="ic"></i></button>';
                    }
                    var html = '<tr class="table-info">' + '<td>' + button + title + '</td>' + '<td>' + dataBook.authors + '</td>' +
                        '<td>' + dataBook.published + '</td>' + '</tr>';
                    $('tbody').append(html);
                }
            },
            error: function (error) {
                alert("Halaman buku tidak ditemukan")
            }
        })
    })
    
});

var addFavorite = function (title, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {id: id},
        success: function (hasil) {
        var button = '<button' + ' id="' + hasil.id + '"' + ' class="bttnn"' + ' onClick="deleteFavorite(\'' + hasil.id + '\')">' +'<i class="del"></i></button>';
            $("#"+hasil.id).replaceWith(button);
            html = "<div id='" + hasil.id + "'>" + hasil.hasil + "</div>";
            $(".data").append(html);
            $(".count").replaceWith("<span class='count'>Add to Favorites: " + hasil.count + "</span>")
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};
var deleteFavorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/delete",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {id: id},
        success: function (count) {
            var button = '<button' + ' id="' + id + '"' + ' class="bttnn"' + ' onClick="addFavorite(\'' +'<i class="ic"></i></button>';
            $("tbody").find("#"+id).replaceWith(button);
            $(".count").replaceWith("<span class='count'>Add to Favorites: " + count.count + "</span>");
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};

$(document).ready(function () {
       gapi.load('auth2', function() {
        gapi.auth2.init();
      });
});