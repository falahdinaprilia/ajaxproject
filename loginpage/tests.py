from django.test import TestCase, Client


# Create your tests here.
class Lab11UnitTest(TestCase):

    def test_lab11_not_logged_landing_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_lab11_false_token(self):
        response = Client().post('/login', {'id_token': "testtt"}).json()
        self.assertEqual(response['status'], "1")

    def test_lab11_right_token(self):
        response = Client().post('/login', {'id_token': "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTA4MzY1NTIwNDE1NC1mZzAwdjRobnVhZGk3a3E4dDZvZzFvdXZtZmsycHQ5dS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImF1ZCI6IjEwODM2NTUyMDQxNTQtZmcwMHY0aG51YWRpN2txOHQ2b2cxb3V2bWZrMnB0OXUuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDE0OTY1NTg1ODcwMzc3NzA0NjMiLCJlbWFpbCI6ImZhbGFocHJpbGlhQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoidnprY1VUN0ZTSTJIUkNxQVV6Q2xPUSIsIm5hbWUiOiJGYWxhaGRpbmEgQSIsInBpY3R1cmUiOiJodHRwczovL2xoNi5nb29nbGV1c2VyY29udGVudC5jb20vLVhMM3BNODhzTEgwL0FBQUFBQUFBQUFJL0FBQUFBQUFBQVlvL1BUVGhHZ0xsNzJJL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJGYWxhaGRpbmEiLCJmYW1pbHlfbmFtZSI6IkEiLCJsb2NhbGUiOiJpZCIsImlhdCI6MTU0MzU4MTA5NSwiZXhwIjoxNTQzNTg0Njk1LCJqdGkiOiI2ODUxZWFmNzhjNGM3ZDczMzAwOTM1ZTliNWQ1ODUzNjVkNmExNDZhIn0.LkcNYMlTLyzttJ9oApE3KVc3b-CXmPVDknndhHw5oH36dnVDbEi883Hu-sjxzv8AOjF-08aejtFvhGzXbdPw2elsHmCqATl_cXCxNBMOipnxiBW_v4HdZzjPNoDHfKMFL1AUVPwjPuPLnBCe9mL3HDETLe40yIwq-bo9WjYFaxNE1rLhd12WvDqXXgeW_o21ybJR60eRVJolkgc2mCmoJUNUt3x3uJ35JR3K8LdBcQeji77nZ_Xvs2Jv3QqbtZdzpHZzYLvVildamCH1NOoy0reliLgZWlRIUUOP4p8CAwTXPhXBjFi8rlvOF1-FN-847-86AMEzmCd52tloB4ehVA"}).json()
        self.assertEqual(response['status'], "0")

    def test_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 302)
