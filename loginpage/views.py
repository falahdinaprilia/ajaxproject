from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests
# Create your views here.
from django.urls import reverse


def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            # print(token)
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), "1083655204154-fg00v4hnuadi7kq8t6og1ouvmfk2pt9u.apps.googleusercontent.com")
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = idinfo['sub']
            email = idinfo['email']
            name = idinfo['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            return JsonResponse({"status": "0", 'url': reverse("homepage")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'loginpage.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))